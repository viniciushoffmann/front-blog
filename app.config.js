blogClient.config(function($routeProvider) {
  
  $routeProvider
  .when("/", {
    templateUrl : "main.html"    
  })
  .when("/posts", {
    templateUrl : "post.html",
    controler: "PostController"
  })
  .when("/tags", {
    templateUrl : "tag.html",
    controler: "blogClient"
  })
  .when("/users", {
    templateUrl : "user.html",
    controler: "blogClient"
  })
  .otherwise({
    templateUrl : "main.html"
  });
});