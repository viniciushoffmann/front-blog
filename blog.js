blogClient.controller('BlogController', ['$scope','notify','$http','$rootScope', function($scope, notify, $http, $rootScope) {

    $scope.users = [];
    $scope.tags =[];
    $scope.posts = [];
    $scope.comments = [];
    $scope.name = 'nome original';
    $scope.page = 1;
    $scope.nfClick = function(name){
    $scope.name = name;
    };

    $scope.isAuthenticated = false;
    $scope.message = notify;


    

    // para pegar os pots 
    $scope.getPosts = function() {
        if($rootScope.isAuthenticated){
        $http({
            method: 'get',
            url: 'http://localhost:3000/posts',
            params:{
                "page": $scope.page
            }       
        }).then(function successCallback(responce){
            $scope.posts = $scope.posts.concat(responce.data);
            $scope.getTags();
            $scope.getUsers();          
        })}
    };
    // para carregar mais post 
    $scope.getMorePosts =function(){
        $scope.page = $scope.page + 1;
        $scope.getPosts();
    }

    // para pegar os usuarios 
    $scope.getUsers = function() {
        
        if($rootScope.isAuthenticated){
        $http({           
            method: 'get',
            url: 'http://localhost:3000/users',
            
            
        }).then(function successCallback(responce){
            $scope.users = responce.data;
        })}
    };
    $scope.getUsers();

    // para pegar os comentarios 
    $scope.getComment = function(post) {
        
        $http({
            method: 'get',
            url: 'http://localhost:3000/posts/'+post.id+'/comments'
        }).then(function successCallback(responce){
            post.comments = responce.data;
        })
    };
    
    // para pegar as tags 
    $scope.getTags = function() {
        $http({
            method: 'get',
            url: 'http://localhost:3000/tags'
        }).then(function successCallback(responce){
            $scope.tags = responce.data;
        })
    };

    if ($rootScope.isAuthenticated){  //testando se tem algum logado para poder criar qualquer coisa


    // para criar novas tags 
    $scope.createTags = function(){
        $http({
            method: 'post',
            url: 'http://localhost:3000/tags',
            data:{
                "name": $scope.tag_name
            }    
        }).then(function(response){
            $scope.tags.push(response.data);
            $scope.tag_name = undefined;
        })
            
    };

    //  para pegar os posts que tem uma determinada tag "usado no primeiro formato de posts nao usado mais" 
    $scope.postsbytag = function(posts, tag_id){
            return $scope.posts.filter(p => p.tags && p.tags.map(t => t.id).includes(tag_id) )
    }
    //  para ver se a tag  tem posts se não tiver ela não mostra a tag por não ter um post  
    $scope.hasPosts = function (tag_id) {
        return $scope.posts.filter(p => p.tags && p.tags.map(t => t.id).includes(tag_id)).length > 0;
    };
    //  para criar novos usuarios 
    $scope.createUsers = function(){
        $http({
            method: 'post',
            url: 'http://localhost:3000/users',
            data:{user:{
                "name": $scope.user_name,
                "username": $scope.user_username,
                "email": $scope.user_email,
                "password": $scope.user_password,
                "password_confirmation": $scope.user_confirmation_password,
                }
            }    
        }).then(function(){
            $scope.getUsers();
            $scope.user_name = undefined;
            $scope.user_username = undefined;
            $scope.user_email = undefined;
            $scope.user_password = undefined;
            $scope.user_password_confirmation = undefined;
        })    
    };
}
    // para criar comentario dos posts

    $scope.createComments = function(post){
        $http({
            method: 'post',
            url: 'http://localhost:3000/posts/'+post.id+'/comments',
            data:{
                "comment":{
                    "text": post.comment_text,
                    "user_id":  JSON.parse(localStorage.getItem('user_id')),
                }               
            }    
        }).then(function(response){
            post.comments.push(response.data)
        })
      
    };
    //  para criar comentario dos comentarios
    $scope.createCommentsComment = function(comment, post){
        $http({
            method: 'post',
            url: 'http://localhost:3000/comments/'+comment.id+'/comment',
            data:{
                "comment":{
                    "text": comment.comment_text,
                    "user_id":  JSON.parse(localStorage.getItem('user_id')),
                    "comment_id": comment.comment_id,
                }               
            }    
        }).then(function(response){
            post.comments.push(response.data)
        })      
    };



    //  para deletar o post selecionado
    $scope.deletePost = function(post){
        $http({
            method: 'delete',
            url: 'http://localhost:3000/posts/'+post.id
        }).then(function(){
            post = $scope.getPosts(post)
        })
    };
    
    // para deletar o comment selecionado
    $scope.deleteComment = function(comment, post){
        $http({
            method: 'delete',
            url: 'http://localhost:3000/comments/'+comment.id
        }).then(function(){
            post.comments = $scope.getComment(post)
        })
    };
    // para deletar o usuario selecionado
    $scope.deleteUsers = function(id){
        $http({
            method: 'delete',
            url: 'http://localhost:3000/users/'+id
        }).then(function(){
            users = $scope.getUsers()
        })
    };
    // para deletar a tag selecionado
    $scope.deleteTags = function(id){
        $http({
            method: 'delete',
            url: 'http://localhost:3000/tags/'+id
        }).then(function(){
            tags = $scope.getTags()
        })
    };

    // para salvar a tag
    $scope.saveTags = function(id){
        $http({
            method: 'put',
            url: 'http://localhost:3000/tags/'+id,
            data:{
                "name": $scope.tagName
            } 
        }).then(function(){
            tags = $scope.getTags()
            $scope.tagId = undefined;
            $scope.tagName = undefined;
        })
    };
    // aqui atribui as variaveis a outra para editar
    $scope.editeTags = function(tag){
        $scope.tagId = tag.id;
        $scope.tagName = tag.name;
        
    }
    // para salvar o usuario
    $scope.saveUsers = function(id){
        $http({
            method: 'put',
            url: 'http://localhost:3000/users/'+id,
            data:{user:{
                "name": $scope.userName,
                "username": $scope.userUsername,
                "email": $scope.userEmail,
                "password": $scope.userPassword,
                "password_confirmation": $scope.userPassword_confirmation,
            }  }  
        }).then(function(){
            $scope.getUsers();
            $scope.userId = undefined;     // undefined serve pra limpar o campo da variavel
            $scope.userName = undefined;
            $scope.userEmail =undefined;
            $scope.userPassword = undefined;
            $scope.userPassword_confirmation = undefined;

        })    
    };
    // aqui atribui as variaveis a outra para editar
    $scope.editeUsers = function(user){
        $scope.userId = user.id;
        $scope.userName = user.name;
        $scope.userUsername = user.username;
        $scope.userEmail = user.email;
        

    }
   

    $scope.getPosts(1);


    $scope.showComments = function(flag, post){
        post.visivel = flag
    };

    $scope.formSubmit = function() {
        $http({
            method: 'post',
            url: 'http://localhost:3000/auth/login',
            data:{
                "email": $scope.user_emaillogin,
                "password": $scope.user_passwordlogin
            }
        }).then(function successCallback(response){
            if (response.data.token){      
                //Aqui estamos adicionando um token jwt no header de todos os requests das solicitações feitas via $http
                $http.defaults.headers.common.Authorization = 'Coders ' + response.data.token;
                $rootScope.isAuthenticated = true;
                $rootScope.token = response.data.token;
                window.localStorage.setItem('user_id', response.data.user_id); 
                $scope.getPosts();
                $scope.getUsers();
                $scope.user_emaillogin = undefined;
                $scope.user_passwordlogin = undefined;
            }

        });
    };
    $scope.logout = function(){
        $http.defaults.headers.common.Authorization = '';
        $rootScope.isAuthenticated = false;
       
    }
    }])


// -----------------------------------------------------controler dos posts-------------------------------------------------





blogClient.controller('PostController', ['$scope', '$http', function($scope, $http) {
    $scope.posts = [];
    $scope.users = [];
    $scope.tags =[];
  
    $scope.getPosts = function() {
        $http({
            method: 'get',
            url: 'http://localhost:3000/posts'
        }).then(function successCallback(responce){
            $scope.posts = responce.data;
        });
    };

    $scope.getPosts();

    // para pegar os usuarios 
    $scope.getUsers = function() {
        $http({
            method: 'get',
            url: 'http://localhost:3000/users'
        }).then(function successCallback(responce){
            $scope.users = responce.data;
        })
    };
    $scope.getUsers();
  
    // para pegar as tags 
    $scope.getTags = function() {
        $http({
            method: 'get',
            url: 'http://localhost:3000/tags'
        }).then(function successCallback(responce){
            $scope.tags = responce.data;
        })
    };
    $scope.getTags()
    
    $scope.createPosts = function(post){
        $http({
            method: 'post',
            url: 'http://localhost:3000/posts',
            data:{
                "post": {
                    "title": $scope.post_title,
                    "description": $scope.post_description,
                    "tag_id": $scope.post_tag,
                    "user_id": JSON.parse(localStorage.getItem('user_id')),
                }
            }    
        }).then(function(response){
            $scope.posts.push(response.data);
            $scope.post_title = undefined;
            $scope.post_description = undefined;
            $scope.post_tag = undefined;
            $scope.post_user = undefined;
        })        
    };


    $scope.postsbytag = function(posts, tag_id){
            return $scope.posts.filter(p => p.tags && p.tags.map(t => t.id).includes(tag_id) )
    }
        
    $scope.deletePost = function(id){
        $http({
            method: 'delete',
            url: 'http://localhost:3000/posts/'+id
        }).then(function(){
            $scope.getPosts()
        })
    };

    $scope.savePosts = function(id){
        $http({
            method: 'put',
            url: 'http://localhost:3000/posts/'+id,
            data:{"post":{
                "title": $scope.postTitle,
                "descripiton": $scope.postDescription,              
                "user_id": JSON.parse(localStorage.getItem('user_id')),
                "tag_id": $scope.postTag,
            }} 
        }).then(function(){
            $scope.getPosts();
            $scope.postId = undefined;
            $scope.postTitle = undefined;
            $scope.postDescription =undefined;
            $scope.postUser = undefined;
            $scope.postTag = undefined;
        })    
    };

    $scope.editePosts = function(post){   
        $scope.postId = post.id;
        $scope.postTitle = post.title;
        $scope.postDescription = post.description;
        $scope.postUser = post.user;
        $scope.postTag = post.tag;  
    }
}])
.factory('notify', [function() {
    return 'message';
        }])
.directive('message', function(){
    return {
        scope: {
            letMessage: '='
        },
        template: '<p class="red">{{letMessage}}</p>'
    }
})
.directive('comments', function () {
    return {
        scope: {
            commentList: '=',
            post: '='
        },
        controller: 'BlogController',
        templateUrl: 'comments.html'
    }
});

